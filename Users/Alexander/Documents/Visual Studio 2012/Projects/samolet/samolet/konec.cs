﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace samolet
{
    public partial class konec : Form
    {
        Timer t = new Timer();
        int budzhet;
        public konec(int budzh)
        {
            InitializeComponent();
            budzhet = budzh;
            label3.Text += "-" + budzhet.ToString();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            t.Interval = 400;
            t.Tick += new EventHandler(t_Tick);
            t.Start();
        }
        void t_Tick(object sender, EventArgs e)
        {
            label2.Visible = !label2.Visible;
        }

        private void konec_KeyDown(object sender, KeyEventArgs e)
        {
            this.Close();
            Application.Restart();
        }
    }
}
