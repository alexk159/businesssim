﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace samolet
{
    public partial class play : Form
    {
        int time;
        int budzhet;
        DateTime date;
        DateTime playdate;
        bool l=false;        
        public play()
        {
            InitializeComponent();
            sekundomer();
            uskvremya();
            button4.Enabled = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {

        }

        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            time = trackBar1.Value;
            label4.Text = (time+1).ToString();
        }

        private void x(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
        }
        public void sekundomer()
        {
            date = DateTime.Now;
            System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
            timer.Interval = 10;
            timer.Tick += new EventHandler(tickTimer);
            timer.Start();
        }

        public void uskvremya()
        {
            DateTime start = new DateTime(2015, 11, 26, 0, 0, 0);
            TimeSpan span = DateTime.Now.Subtract(start);
            DateTime now8 = start.AddSeconds(span.TotalSeconds * 8);
            label5.Text = String.Format("{0:HH:mm:ss}", now8);
        }
        private void tickTimer(object sender, EventArgs e)
        {
            long tick = DateTime.Now.Ticks - date.Ticks;
            DateTime stopwatch = new DateTime();

            stopwatch = stopwatch.AddTicks(tick);
            label5.Text = String.Format("{0:HH:mm:ss}", stopwatch);
        }

        private void button2_Click(object sender, EventArgs e)
        {
                l = false;
                budzh();
        }

        private void button3_Click(object sender, EventArgs e)
        {
                l = true;
                budzh();
        }
        public void budzh()
        {
            if (l)
            {
                label6.Text = "-";
                label7.Text = "30.000$";
                label6.Refresh();
                label7.Refresh();
                panel3.BackColor = System.Drawing.Color.Red;
                panel3.Refresh();
                budzhet = int.Parse(label3.Text);
                for (int i = 0; i <= 30000; i += 30)
                {
                    label3.Text = (budzhet - i).ToString();
                    if (int.Parse(label3.Text) < 0)
                    {
                        break;
                    }
                    label3.Refresh();
                    
                }
            }
            else
            {
                label6.Text = "+";
                label7.Text = "50.000$";
                label6.Refresh();
                label7.Refresh();
                panel3.BackColor = System.Drawing.Color.Green;
                panel3.Refresh();
                budzhet = int.Parse(label3.Text);
                for (int i = 0; i <= 50000; i += 50)
                {
                    label3.Text = (budzhet + i).ToString();
                    label3.Refresh();
                }
            }

        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void checkedListBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            listBox1.Items.RemoveAt(listBox1.SelectedIndex);
            l = false;
            budzh();
        }
        public void proverka()
        {
            if((listBox1.SelectedItems.Count==0)||(listBox1.SelectedItems.Count==0))
                button4.Enabled=false;
            else button4.Enabled=true;
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label3_TextChanged(object sender, EventArgs e)
        {
            if (int.Parse(label3.Text) < 0)
            {
                konec kon = new konec(budzhet);
                this.Hide();
                kon.Show();
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            proverka();
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            proverka();
        }
    }
}
